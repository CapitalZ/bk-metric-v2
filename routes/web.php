<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('api')->group(function() {
    Route::get('players/{input}', 'App\Http\Controllers\API\JsonController@searchPlayers');
    Route::get('players/get/{limit}', 'App\Http\Controllers\API\JsonController@getPlayers');
    Route::get('logs/get/{player_id}', 'App\Http\Controllers\API\JsonController@getPlayerLogs');
    Route::get('logs/{per_page?}', 'App\Http\Controllers\API\JsonController@getLogs');
});
