function getPlayers(limit) {
    console.log('[BKM] Fetching default player list');
    $.ajax({
        url: "/api/players/get/"+limit,
        type: "GET",
        contentType: "application/json;",
        cache: false,
        success: function (players) {
            updatePlayerList(players);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
        }
    })
}

function playerSearch(query) {
    console.log('[BKM] Fetching players with query '+query.toString());
    $.ajax({
        url: "/api/players/"+query.toString(),
        type: "GET",
        contentType: "application/json;",
        cache: false,
        success: function (players) {
            updatePlayerList(players)
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
        }
    })
}

function getPlayerLogs(player_id) {
    console.log('[BKM] Fetching logs for player with ID '+player_id.toString());
    $.ajax({
        url: "/api/logs/get/"+player_id.toString(),
        type: "GET",
        contentType: "application/json;",
        cache: false,
        success: function (logs) {
            updatePlayerLogs(logs);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
        }
    })
}

function getEventTimer() {
    console.log('[BKM] Fetching event timer');
    $.ajax({
        url: "//api.badkush.com/event.json",
        type: "GET",
        cache: false,
        success: function (response) {
            if(response.time == 'now') {
                $('.bkm-event-timer').html("<span class='d-none d-sm-inline-block'>Boss Event is</span> <b>active</b>");
            } else {
                var relativeTime;
                if(response.time > 60)
                    relativeTime = '1 hour and '+(response.time - 60)+' minutes';
                else
                    relativeTime = response.time+' minutes';
                $('.bkm-event-timer').html("<span class='d-none d-sm-inline-block'>Boss Event in</span> <b>"+relativeTime+"</b>");
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
        }
    })
}

function getPlayerCount() {
    console.log('[BKM] Fetching player count');
    $.ajax({
        url: "//api.badkush.com/players.json",
        type: "GET",
        cache: false,
        success: function (response) {
            if(response.players > 1)
                $('.bkm-player-count').html("<b>"+response.players+"</b> <span class='d-none d-sm-inline-block'>players online</span>");
            else
                $('.bkm-player-count').html("<b>"+response.players+"</b> <span class='d-none d-sm-inline-block'>player online</span>");
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
        }
    })
}

function getLogs(url) {
    console.log('[BKM] Fetching logs with pagination');
    $.ajax({
        url: url,
        type: "GET",
        cache: false,
        success: function (response) {
            console.log(response);
            // Update logs
            $('.bkm-log-container').html('');
            $(response.data).each(function (i, log) {
                $('.bkm-log-container').append(
                    '<div class="bkm-panel bkm-log">\n' +
                    '<div class="bkm-img-container">\n' +
                    '<img src="//www.badkush.nl/sprites/items/items/'+log.item_id+'.png" alt="'+log.label+'">\n' +
                    '</div>\n' +
                    '<div class="bkm-log-text">\n' +
                    '<p><b class="bkm-get-player-log" onclick="getPlayerLogs('+log.player_id+')">'+log.ign+'</b> received <b>'+log.label+'</b></p>\n' +
                    '<span>'+log.created_at+'</span>\n' +
                    '</div>\n' +
                    '</div>'
                );
            });

            // Update pagination
            $('.pagination').html('');
            $(response.links).each(function (i, link) {
                var isActive = '', mobileOnly = '';

                if(link.active)
                    isActive = 'active';

                console.log(i);

                if(i > 0 && i < 14)
                    mobileOnly = 'd-none d-md-block';

                $('.pagination').append(
                    '<li class="page-item"><a class="page-link bkm-paginate-to '+mobileOnly+' '+isActive+'" data-url="'+link.url+'" href="#">'+link.label+'</a></li>'
                );
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
            toastr.error(errorThrown, 'Something went wrong...')
        }
    })
}

function updatePlayerList(players) {
    if(players.length > 0) {
        $('.bkm-player-list').html('');
        $(players).each(function (i, player) {
            var isOnline = 'inactive', playerRank = '';

            if(player.online)
                isOnline = 'active';

            if(player.rank)
                playerRank = '<img src="/sprites/crowns/'+player.rank+'.png">'

            $('.bkm-player-list').append(
                '<a href="#" class="bkm-player">\n' +
                '<i class="bkm-dot '+isOnline+'"></i>\n' +
                '<p>'+player.name+'</p>\n' +
                playerRank+'\n' +
                '<div class="bkm-player-collapse">\n' +
                '<button class="btn p-0 mt-1 me-3" onclick="getPlayerLogs('+player.id+')">\n' +
                '<i class="fal fa-coins me-2"></i>\n' +
                '</button>\n' +
                '<button onclick="window.open(\'https://highscores.badkush.com/?username='+player.name+'\')" class="btn p-0 mt-1">\n' +
                '<i class="fal fa-chart-line me-2"></i>\n' +
                '</button>\n' +
                '<span class="d-block"><small><b>Last seen '+player.last_seen+'</b></small></span>' +
                '</div>'+
                '</a>'
            );
        });
    } else {
        toastr.warning('Player could not be found', 'Uh-oh...')
        getPlayers(25);
    }
}


function updatePlayerLogs(logs) {
    console.log(logs);
    if(logs.length > 0) {
        $('.bkm-log-container').html('');
        $(logs).each(function (i, log) {
            $('.bkm-log-container').append(
                '<div class="bkm-panel bkm-log">\n' +
                '<div class="bkm-img-container">\n' +
                '<img src="//www.badkush.nl/sprites/items/items/'+log.item_id+'.png" alt="Coins">\n' +
                '</div>\n' +
                '<div class="bkm-log-text">\n' +
                '<p><b>'+log.player_name+'</b> received <b>'+log.item_label+'</b></p>\n' +
                '<span>'+log.time_ago+'</span>\n' +
                '</div>\n' +
                '</div>'
            );
        });
    } else {
        toastr.warning('No records found', 'Uh-oh...');
        // getPlayers(25);
    }
}

