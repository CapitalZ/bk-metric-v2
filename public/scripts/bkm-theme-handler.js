$(document).ready(function () {
    var activeTheme = 'dark';

    let themes = {
        'dark': {
            'primary-color': '#DD524C',
            'secondary-color': '#DD524C',
            'bg-color': '#121826',
            'panel-color': '#212936',
            'text-color': '#E5E7EB'
        },
        'light': {
            'primary-color': '#DD524C',
            'secondary-color': '#DD524C',
            'bg-color': '#e5e7eb',
            'panel-color': '#ffffff',
            'text-color': '#202938'
        }
    };

    var r = document.querySelector(':root');

    $('.bkm-toggle-light-mode').on('click', function() {
        event.preventDefault();

        if(activeTheme == 'dark') {
            $(this).find('span').text('Light');
            r.style.setProperty('--primary-color', themes['light']['primary-color']);
            r.style.setProperty('--secondary-color', themes['light']['secondary-color']);
            r.style.setProperty('--bg-color', themes['light']['bg-color']);
            r.style.setProperty('--panel-color', themes['light']['panel-color']);
            r.style.setProperty('--text-color', themes['light']['text-color']);
            activeTheme = 'light';
        } else {
            $(this).find('span').text('Dark');
            r.style.setProperty('--primary-color', themes['dark']['primary-color']);
            r.style.setProperty('--secondary-color', themes['dark']['secondary-color']);
            r.style.setProperty('--bg-color', themes['dark']['bg-color']);
            r.style.setProperty('--panel-color', themes['dark']['panel-color']);
            r.style.setProperty('--text-color', themes['dark']['text-color']);
            activeTheme = 'dark';
        }
    });

    $('.bkm-set-theme-color').on('click', function () {
        var color = $(this).data('color');
        r.style.setProperty('--primary-color', color);
        r.style.setProperty('--secondary-color', color);
    });
});
