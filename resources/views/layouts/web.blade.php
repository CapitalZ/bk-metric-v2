<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>BK Metric V2 - @yield('title')</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ url('/fontawesome/css/all.css') }}">

    <!-- Toastr -->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ url('style/main.css') }}">
</head>
<body>
<nav class="bkm-top-bar">
    <div class="container-fluid">
        <ul class="bkm-top-bar-item">
            <li><a href="#" class="bkm-toggle-light-mode"><i class="fal fa-moon"></i> <span>Dark</span></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" id="dropdownThemeSelector" data-bs-toggle="dropdown" aria-expanded="false"><i class="fal fa-palette"></i> <span>Theme</span></a>
                <ul class="dropdown-menu bkm-color-selector" aria-labelledby="dropdownThemeSelector">
                    <li><a class="dropdown-item bkm-set-theme-color bkm-color-1" data-color="#DD524C" href="#"><i class="fas fa-square"></i></a></li>
                    <li><a class="dropdown-item bkm-set-theme-color bkm-color-2" data-color="#43946C" href="#"><i class="fas fa-square"></i></a></li>
                    <li><a class="dropdown-item bkm-set-theme-color bkm-color-3" data-color="#0080d2" href="#"><i class="fas fa-square"></i></a></li>
                </ul>
            </li>
        </ul>
        <ul class="bkm-top-bar-item bkm-tb-left">
            <li><i class="fal fa-exclamation-circle align-middle me-1"></i> <small class="bkm-event-timer"></small></li>
            <li><i class="fal fa-user align-middle me-1"></i> <small class="bkm-player-count"></small></li>
{{--            <li><a href="https://discord.com/invite/jSdQYSV" target="_blank"><i class="fab fa-discord"></i></a></li>--}}
            {{--            <li><a href="#"><i class="fab fa-discord"></i></a></li>--}}
{{--            <li><a href="#"><i class="fab fa-discord"></i></a></li>--}}
        </ul>
    </div>
</nav>

@yield('content')

@include('shared.errors')
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="{{ url('/scripts/bkm-theme-handler.js') }}"></script>
<script src="{{ url('/scripts/bkm-data-handler.js') }}"></script>
@yield('scripts')
</body>
</html>
