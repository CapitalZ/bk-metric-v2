@extends('layouts.web')
@section('title', 'Homepage')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 mb-5 mb-lg-0">
                <div class="bkm-panel">
                    <input type="text" class="form-control bkm-input mb-1" id="search" placeholder="Search a player (Press “ESC” to focus)">
                    <div class="bkm-player-list">
                        <i class="fas fa-circle-notch fa-spin fa-2x bkm-loading-anim"></i>
{{--                        <a href="#" class="bkm-player">--}}
{{--                            <i class="bkm-dot active"></i>--}}
{{--                            <p>name</p>--}}
{{--                            <img src="/sprites/crowns/1.png">--}}
{{--                            <div class="bkm-player-collapse">--}}
{{--                                <button class="btn p-0 mt-1 me-3">--}}
{{--                                    <i class="fal fa-coins me-2"></i>--}}
{{--                                </button>--}}
{{--                                <button class="btn p-0 mt-1">--}}
{{--                                    <i class="fal fa-chart-line me-2"></i>--}}
{{--                                </button>--}}
{{--                                <span class="d-block"><small><b>Last seen 1 minute ago</b></small></span>--}}
{{--                            </div>--}}
{{--                        </a>--}}
                    </div>
                    <ul class="bkm-pagination-section">
                        <span class="bkm-label">
                            Show
                        </span>
                        <li class="page-item">
                            <a class="page-link bkm-toggle-show-qty" href="#" data-qty="4">4</a>
                        </li>
                        <li class="page-item">
                            <a class="page-link bkm-toggle-show-qty" href="#" data-qty="8">8</a>
                        </li>
                        <li class="page-item">
                            <a class="page-link bkm-toggle-show-qty active" href="#" data-qty="12">12</a>
                        </li>
                    </ul>
                    <ul class="bkm-pagination-section float-end">
                            <span class="bkm-label">
                                Display
                            </span>
                        <li class="page-item">
                            <a class="page-link bkm-toggle-display" href="#" data-mode="list"><i class="fal fa-list"></i></a>
                        </li>
                        <li class="page-item">
                            <a class="page-link bkm-toggle-display active" href="#" data-mode="table"><i class="fal fa-table"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 mb-5 mb-lg-0 position-relative">
                <div class="bkm-log-container bkm-display-table">
{{--                    <div class="bkm-panel bkm-log">--}}
{{--                        <div class="bkm-img-container">--}}
{{--                            <img src="//www.badkush.nl/sprites/npcs/monster/Zulrah.png" alt="Coins">--}}
{{--                        </div>--}}
{{--                        <div class="bkm-log-text">--}}
{{--                            <p><b>An Enraged Zulrah</b> has spawned</p>--}}
{{--                            <span>23 minutes ago</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
                <nav aria-label="Logs pagination" class="bkm-pagination-container position-absolute bottom-0">
                    <ul class="pagination">
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                                <span aria-hidden="true"><i class="fal fa-chevron-left"></i></span>
                            </a>
                        </li>
                        <li class="page-item"><a class="page-link active" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                                <span aria-hidden="true"><i class="fal fa-chevron-right"></i></span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            var hasLoaded = false;

            if(!hasLoaded) {
                getPlayers(25);
                getEventTimer();
                getPlayerCount();
                getLogs("/api/logs/12");
                hasLoaded = true;
            }
        });
        $(document).on('keydown', function (e) {
            if (e.keyCode === 27) {
                $('#search').focus();
            }
        });
        $(document).on('click', '.bkm-player', function() {
            event.preventDefault();
            $('.bkm-player-collapse').removeClass('expanded');
            $(this).find('.bkm-player-collapse').addClass('expanded');
        });
        $(document).on('click', '.bkm-paginate-to', function() {
            event.preventDefault();
            getLogs($(this).data('url'))
        });
        $('#search').keypress(function (event) {
            if(event.keyCode == 13) {
                if($(this).val() === '')
                    getPlayers(25);
                else
                    playerSearch($(this).val())
            }
        });
        $('#search').on('change', function () {
            if($(this).val() === '')
                getPlayers(25);
            else
                playerSearch($(this).val())
        });
        var bossTimer = setInterval(function () {
            getEventTimer();
            getPlayerCount();
        }, 3e4);
        $('.bkm-toggle-show-qty').on('click', function () {
            event.preventDefault();
            $('.bkm-toggle-show-qty').removeClass('active');
            $(this).addClass('active');
            getLogs('/api/logs/'+$(this).data('qty'));
        });
        $('.bkm-toggle-display').on('click', function () {
            event.preventDefault();
            $('.bkm-toggle-display').removeClass('active');
            $(this).addClass('active');

            if($(this).data('mode') == 'table')
                $('.bkm-log-container').addClass('bkm-display-table');
            else
                $('.bkm-log-container').removeClass('bkm-display-table');
        });
    </script>
@endsection
