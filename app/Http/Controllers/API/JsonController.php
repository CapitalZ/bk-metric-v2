<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Support\Jsonable;

class JsonController extends Controller
{
    public function getPlayers($limit)
    {
        $results = DB::table('players')->orderBy('updated_at', 'DESC')->orderBy('online', 'DESC')->limit($limit)->get();
        $players = [];
        // Hello, world
        foreach($results as $result) {
            array_push($players, [
                'id' => $result->id,
                'name' => $result->ign,
                'online' => $result->online,
                'rank' => $result->rank,
                'last_seen' => Carbon::parse($result->updated_at)->diffForHumans()
            ]);
        }

        return response()->json($players);
    }
    public function searchPlayers($input) {
        $results = DB::table('players')->where('ign', 'LIKE', '%'.$input.'%')->get();
        $players = [];
        foreach($results as $result) {
            array_push($players, [
                'id' => $result->id,
                'name' => $result->ign,
                'online' => $result->online,
                'rank' => $result->rank,
                'last_seen' => Carbon::parse($result->updated_at)->diffForHumans()
            ]);
        }

        return response()->json($players);
    }

    public function getPlayerLogs($player_id, $per_page = 4, $page = 1)
    {
        // TODO: Add pagination + results per page
        $results = DB::table('game_log')
            ->where('game_log.player_id', '=', $player_id)
            ->join('itemlist', 'game_log.item_id', '=', 'itemlist.item_id')
            ->join('players', 'game_log.player_id', '=', 'players.id')
            ->select('game_log.created_at', 'game_log.output', 'itemlist.label', 'itemlist.rarity', 'itemlist.item_id', 'players.ign')
            ->orderBy('game_log.created_at', 'DESC')
            ->get();

        $logs = [];
        foreach($results as $result) {
            array_push($logs, [
                'player_name' => $result->ign,
                'output' => $result->output,
                'item_id' => $result->item_id,
                'item_label' => $result->label,
                'item_rarity' => $result->rarity,
                'time_ago' => Carbon::parse($result->created_at)->diffForHumans()
            ]);
        }

        return response()->json($logs);
    }

    public function getLogs($per_page = 4)
    {

        $results = DB::table('game_log')
            ->join('itemlist', 'game_log.item_id', '=', 'itemlist.item_id')
            ->join('players', 'game_log.player_id', '=', 'players.id')
            ->select('game_log.created_at', 'game_log.output', 'itemlist.label', 'itemlist.rarity', 'itemlist.item_id', 'players.id AS player_id', 'players.ign')
            ->orderBy('game_log.created_at', 'DESC')
            ->paginate($per_page);

        return response()->json($results);
    }
}
